/**
 * 
 */
$(function() {
	// 原始代码
	$('#old_pswd').validatebox({
		required : true,
		validType : [ "length[6,100]", "not_chinese", "remote[basePath + 'mainController/pswdCheck', 'oldPswd']" ]
	});

	// 新改密码
	$('#new_pswd').validatebox({
		required : true,
		validType : [ "length[6,100]", "not_chinese" ]
	});

	// 新改密码
	$('#pswf_confim').validatebox({
		required : true,
		validType : [ "length[6,100]", "not_chinese", "same['new_pswd']" ]
	});

	// 保存按钮
	$('#pswd_save_btn').linkbutton({});

	// 绑定保存按钮事件
	$('#pswd_save_btn').click(function() {
		save();
	});

	// 保存方法
	function save() {
		alert('修改......');
	}
});